# Add directory from Tool 'bin' to PATH (Manages binary files downloaded from different sources https://github.com/marcosnils/bin)
export PATH=$HOME/bin:$PATH
# Add go to PATH
export PATH=$PATH:$HOME/go/bin:/usr/local/go/bin
